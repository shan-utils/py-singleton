#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
sys.path.insert(0, '..')

from py_singleton import Singleton


class Test(metaclass=Singleton):
    def __init__(self):
        self.attr1 = 10
        self.attr2 = 'abc'


test = Test()
print('Before change:', test.attr1)
test.attr1 = 20
print('After change:', test.attr1)

test = Test()
print('Reuse:', test.attr1)
