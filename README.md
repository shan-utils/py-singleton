# Singleton Design Pattern in Python

## Usage:

```python
from py_singleton import Singleton

class Test(metaclass=Singleton):
    def __init__(self):
        self.attr1 = 10

test = Test() # test.attr1 == 10
test.attr1 = 20 # test.attr1 == 20
test = Test() # test.attr1 == 20 rather than 10

```

## Note:
Right now, the first initialization supports passing `args` to `__init__`, such as `test = Test(attr1=10)`. Passing `args` when *reusing* does *not* any effects; rather, use `test.attr1 = 20` after reinitialization `test = Test()` to change the attributes. This is because if the metaclass `Singleton` calls `self._instance.__init__`, it will overwrite other attributes defined in `__init__` of the instance.
