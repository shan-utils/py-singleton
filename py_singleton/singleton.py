# -*- coding: utf-8 -*-

class Singleton(type):
    """Singleton pattern

    There is only one instance of a singleton class

    """
    _instance = None
    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = super().__call__(*args, **kwargs)
        # else:
        #     self._instance.__init__(*args, **kwargs)
        return self._instance
