# -*- coding: utf-8 -*-

from distutils.core import setup
from glob import glob

scripts = glob('scripts/*')

setup(name='py-singleton',
      version='0.0.1',
      description='Singleton design pattern in Python',
      author='Shuo Han',
      author_email='shan50@jhu.edu',
      packages=['py_singleton'],
      scripts=scripts)
